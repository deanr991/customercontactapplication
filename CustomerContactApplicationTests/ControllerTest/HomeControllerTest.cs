﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomerContactApplication.Controllers;
using CustomerContactApplication.WebAPI;
using Moq;
using System.Web.Mvc;
using CustomerContactApplication.Models;

namespace UnitTests.ControllerTest
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void TestIndexView()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();

            //Act
            var controller = new HomeController(apiMock.Object);
            var result = controller.Index() as ViewResult;

            //Assert
            Assert.AreEqual("Index", result.ViewName);
        }

        [TestMethod]
        public void TestSearchView()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();

            //Act
            var controller = new HomeController(apiMock.Object);
            var result = controller.Search() as ViewResult;

            //Assert
            Assert.AreEqual("Search", result.ViewName);
        }

        [TestMethod]
        public void TestSubmitPageHasModel()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();

            //Act
            var controller = new HomeController(apiMock.Object);
            var result = controller.Submit() as ViewResult;
            var model = (SubmitDataModel)result.ViewData.Model;

            //Assert
            Assert.AreEqual(model.GetType(), typeof(SubmitDataModel));
        }
    }
}
