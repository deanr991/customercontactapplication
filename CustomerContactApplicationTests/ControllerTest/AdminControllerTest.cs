﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomerContactApplication.Controllers;
using CustomerContactApplication.WebAPI;
using Moq;
using System.Web.Mvc;
using CustomerContactApplication.Models;

namespace UnitTests
{
    [TestClass]
    public class AdminControllerTest
    {
        [TestMethod]
        public void TestPanelView()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();

            //Act
            var controller = new AdminController(apiMock.Object);
            var result = controller.Panel() as ViewResult;

            //Assert
            Assert.AreEqual("Panel", result.ViewName);
        }

        [TestMethod]
        public void TestCreateUserPasswordsDoNotMatch()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();
            var model = new AdminPanelModel();
            model.Password = "First";
            model.PasswordAgain = "Second";

            //Act
            var controller = new AdminController(apiMock.Object);
            var result = controller.CreateUser(model) as ViewResult;
            model = (AdminPanelModel)result.ViewData.Model;

            //Assert
            Assert.AreEqual("The passwords do not match", model.ErrorMessage);
        }

        [TestMethod]
        public void TestGetStatistics()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();

            //Act
            var controller = new AdminController(apiMock.Object);
            var model = controller.GetStatistics(new AdminPanelModel());

            //Assert
            Assert.AreEqual(model.GetType(), typeof(AdminPanelModel));
        }
    }
}
