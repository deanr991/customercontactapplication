﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomerContactApplication.Controllers;
using CustomerContactApplication.WebAPI;
using Moq;
using System.Web.Mvc;
using CustomerContactApplication.Models;

namespace UnitTests
{
    [TestClass]
    public class AuthControllerTest
    {
        [TestMethod]
        public void TestErrorView()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();

            //Act
            var controller = new AuthController(apiMock.Object);
            var result = controller.Error() as ViewResult;

            //Assert
            Assert.AreEqual("Error", result.ViewName);
        }

        [TestMethod]
        public void TestLoginNotAuthenticated()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();
            var contextMock = new Mock<ControllerContext>();
            contextMock.SetupGet(p => p.HttpContext.User.Identity.IsAuthenticated).Returns(false);

            //Act
            var controller = new AuthController(apiMock.Object);
            controller.ControllerContext = contextMock.Object;
            var result = controller.Login() as ViewResult;

            //Assert
            Assert.AreEqual("Login", result.ViewName);
        }

        [TestMethod]
        public void TestLoginAlreadyAuthenticated()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();
            var contextMock = new Mock<ControllerContext>();
            contextMock.SetupGet(p => p.HttpContext.User.Identity.IsAuthenticated).Returns(true);

            //Act
            var controller = new AuthController(apiMock.Object);
            controller.ControllerContext = contextMock.Object;
            var result = (RedirectToRouteResult) controller.Login();

            //Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void TestUnSuccessfulTokenRequest()
        {
            //Arrange
            Mock<IWebAPIServices> apiMock = new Mock<IWebAPIServices>();
            UserDataModel model = new UserDataModel();
            model.username = "username";
            model.password = "password";
            TokenModel token = new TokenModel();
            token.access_token = null;
            token.token_type = null;
            token.expires_in = null;
            token.userId = null;
            apiMock.Setup(p => p.RequestAPIToken(model.username, model.password)).Returns(token);
            var contextMock = new Mock<ControllerContext>();
            contextMock.SetupGet(p => p.HttpContext.User.Identity.IsAuthenticated).Returns(false);

            //Act
            var controller = new AuthController(apiMock.Object);
            controller.ControllerContext = contextMock.Object;
            var result = controller.Login(model) as ViewResult;

            //Assert
            Assert.AreEqual("Login", result.ViewName);
        }
    }
}
