﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Interfaces
{
    public interface IAdoNetContext
    {
        IAdoNetUnitOfWork CreateUnitOfWork();

        IDbCommand CreateCommand();

        void RemoveTransaction(AdoNetUnitOfWork obj);

        void Dispose();
    }
}
