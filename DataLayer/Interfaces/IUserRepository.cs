﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Interfaces
{
    public interface IUserRepository
    {
        IAdoNetContext _context { get; set; }

        void InjectContext(IAdoNetContext context);

        void InsertUser(User user);
        IEnumerable<User> GetUserByUserId(int userId);

        IEnumerable<User> GetUserByUsername(string username);

        IEnumerable<User> GetValidatedUser(string username, string password);
    }
}
