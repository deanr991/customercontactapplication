﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Interfaces
{
    public interface IAddressRepository
    {
        IAdoNetContext _context { get; set; }
        void InjectContext(IAdoNetContext context);

        IEnumerable<Address> InsertAddress(Address address);

        IEnumerable<Address> GetAddressByAddressId(int addressId);
    }
}
