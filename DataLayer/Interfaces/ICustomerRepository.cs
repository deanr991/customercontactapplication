﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Interfaces
{
    public interface ICustomerRepository
    {
        IAdoNetContext _context { get; set; }
        void InjectContext(IAdoNetContext context);

        IEnumerable<Customer> InsertCustomer(Customer customer);

        IEnumerable<Customer> GetCustomerByCustomerId(int customerId);

        IEnumerable<Customer> GetCustomerBySurname(string surname);

        IEnumerable<Customer> GetCustomerByEmail(string email);

        void DeleteCustomer(int customerId);
    }
}
