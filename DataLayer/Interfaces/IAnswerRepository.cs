﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Interfaces
{
    public interface IAnswerRepository
    {
        IAdoNetContext _context { get; set; }

        void InjectContext(IAdoNetContext context);

        void InsertAnswer(Answer answer);

        IEnumerable<Answer> GetAllAnswers();

        IEnumerable<Answer> GetAnswerByCustomerId(int customerId);
    }
}
