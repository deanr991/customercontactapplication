﻿using BusinessEntities.Entities;

namespace DataLayer.Interfaces
{
    public interface IErrorLogRepository
    {
        IAdoNetContext _context { get; set; }

        void InjectContext(IAdoNetContext context);

        void InsertErrorLogEntry(ErrorLog error);
    }
}
