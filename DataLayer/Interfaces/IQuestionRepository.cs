﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Interfaces
{
    public interface IQuestionRepository
    {
        IAdoNetContext _context { get; set; }

        void InjectContext(IAdoNetContext context);

        void InsertQuestion(Question question);
        IEnumerable<Question> GetQuestionByQuestionId(int questionId);

        IEnumerable<Question> GetAllQuestions();
    }
}
