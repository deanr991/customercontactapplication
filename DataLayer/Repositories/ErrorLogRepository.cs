﻿using BusinessEntities.Entities;
using DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    public class ErrorLogRepository : GenericRepository<ErrorLog>, IErrorLogRepository
    {
        public IAdoNetContext _context { get; set; }
        public void InjectContext(IAdoNetContext context)
        {
            _context = context;
        }

        public void InsertErrorLogEntry(ErrorLog error)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.InsertErrorLogEntry";

                command.Parameters.Add(new SqlParameter("@Source", error.Source));
                command.Parameters.Add(new SqlParameter("@Method", error.Method));
                command.Parameters.Add(new SqlParameter("@RequestUri", error.RequestUri));
                command.Parameters.Add(new SqlParameter("@Exception", error.Exception));

                Execute(command);
            }
        }

        protected override void Map(IDataRecord record, ErrorLog entity)
        {
            entity.Id = ConvertFromDBVal<int>(record["Id"]);
            entity.Source = ConvertFromDBVal<string>(record["Source"]);
            entity.Method = ConvertFromDBVal<string>(record["Method"]);
            entity.RequestUri = ConvertFromDBVal<string>(record["RequestUri"]);
            entity.Exception = ConvertFromDBVal<string>(record["Exception"]);
        }
    }
}
