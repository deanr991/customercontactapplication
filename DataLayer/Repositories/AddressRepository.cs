﻿using BusinessEntities.Entities;
using DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    /// <summary>
    /// A medium between the data source and the business logic for working with address entities
    /// </summary>
    public class AddressRepository : GenericRepository<Address>, IAddressRepository
    {
        public IAdoNetContext _context { get; set; }
        public void InjectContext(IAdoNetContext context)
        {
            _context = context;
        }

        public IEnumerable<Address> InsertAddress(Address address)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.InsertAddress";

                command.Parameters.Add(new SqlParameter("@AddressLine1", address.AddressLine1));
                command.Parameters.Add(new SqlParameter("@AddressLine2", address.AddressLine2));
                command.Parameters.Add(new SqlParameter("@City", address.City));
                command.Parameters.Add(new SqlParameter("@Postcode", address.Postcode));
                command.Parameters.Add(new SqlParameter("@Country", address.Country));

                return Execute(command);
            }
        }
        public IEnumerable<Address> GetAddressByAddressId(int addressId)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetAddressByAddressId";

                command.Parameters.Add(new SqlParameter("@AddressId", addressId));

                return Execute(command);
            }
        }

        protected override void Map(IDataRecord record, Address entity)
        {
            entity.AddressId = ConvertFromDBVal<int>(record["AddressId"]);
            entity.AddressLine1 = ConvertFromDBVal<string>(record["AddressLine1"]);
            entity.AddressLine2 = ConvertFromDBVal<string>(record["AddressLine2"]);
            entity.City = ConvertFromDBVal<string>(record["City"]);
            entity.Postcode = ConvertFromDBVal<string>(record["Postcode"]);
            entity.Country = ConvertFromDBVal<string>(record["Country"]);
        }
    }
}
