﻿using BusinessEntities.Entities;
using DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    /// <summary>
    /// A medium between the data source and the business logic for working with customer entities
    /// </summary>
    public class QuestionRepository : GenericRepository<Question>, IQuestionRepository
    {
        public IAdoNetContext _context { get; set; }
        public void InjectContext(IAdoNetContext context)
        {
            _context = context;
        }

        public void InsertQuestion(Question question)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.InsertQuestion";

                command.Parameters.Add(new SqlParameter("@Description", question.Description));

                Execute(command);
            }
        }
        public IEnumerable<Question> GetQuestionByQuestionId(int questionId)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetQuestionByQuestionId";

                command.Parameters.Add(new SqlParameter("@QuestionId", questionId));

                return Execute(command);
            }
        }

        public IEnumerable<Question> GetAllQuestions()
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetAllQuestions";

                return Execute(command);
            }
        }

        protected override void Map(IDataRecord record, Question entity)
        {
            entity.QuestionId = ConvertFromDBVal<int>(record["QuestionId"]);
            entity.Description = ConvertFromDBVal<string>(record["Description"]);
        }
    }
}

