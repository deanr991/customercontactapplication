﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public abstract class GenericRepository<T> where T : new()
    {
        /// <summary>
        /// Executes the stored procedure against the database
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected IEnumerable<T> Execute(IDbCommand command)
        {
            foreach (SqlParameter parameter in command.Parameters)
            {
                if (parameter.Value == null)
                {
                    parameter.Value = DBNull.Value;
                }
            }
            using (var reader = command.ExecuteReader())
            {
                List<T> items = new List<T>();
                while (reader.Read())
                {
                    var item = new T();
                    Map(reader, item);
                    items.Add(item);
                }
                return items;
            }
        }

        /// <summary>
        /// Used to map data records onto the business entities
        /// </summary>
        /// <param name="record"></param>
        /// <param name="entity"></param>
        protected abstract void Map(IDataRecord record, T entity);
        protected static R ConvertFromDBVal<R>(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return default(R); // returns the default value for the type
            }
            else
            {
                return (R)obj;
            }
        }
    }
}
