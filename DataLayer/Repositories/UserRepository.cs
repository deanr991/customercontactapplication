﻿using BusinessEntities.Entities;
using DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    /// <summary>
    /// A medium between the data source and the business logic for working with user entities
    /// </summary>
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public IAdoNetContext _context { get; set; }
        public void InjectContext(IAdoNetContext context)
        {
            _context = context;
        }

        public void InsertUser(User user)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.InsertUser";

                command.Parameters.Add(new SqlParameter("@Username", user.Username));
                command.Parameters.Add(new SqlParameter("@Password", user.Password));
                command.Parameters.Add(new SqlParameter("@Salt", user.Salt));

                Execute(command);
            }
        }
        public IEnumerable<User> GetUserByUserId(int userId)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetUserByUserId";

                command.Parameters.Add(new SqlParameter("@UserId", userId));

                return Execute(command);
            }
        }

        public IEnumerable<User> GetUserByUsername(string username)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetUserByUsername";

                command.Parameters.Add(new SqlParameter("@Username", username));

                return Execute(command);
            }
        }

        public IEnumerable<User> GetValidatedUser(string username, string password)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetValidatedUser";

                command.Parameters.Add(new SqlParameter("@Username", username));
                command.Parameters.Add(new SqlParameter("@Password", password));
                return Execute(command);
            }
        }

        protected override void Map(IDataRecord record, User entity)
        {
            entity.UserId = ConvertFromDBVal<int>(record["UserId"]);
            entity.Username = ConvertFromDBVal<string>(record["Username"]);
            entity.Password = ConvertFromDBVal<string>(record["Password"]);
            entity.Salt = ConvertFromDBVal<string>(record["Salt"]);
        }
    }
}
