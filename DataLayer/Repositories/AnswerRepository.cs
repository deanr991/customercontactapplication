﻿using BusinessEntities.Entities;
using DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    /// <summary>
    /// A medium between the data source and the business logic for working with answer entities
    /// </summary>
    public class AnswerRepository : GenericRepository<Answer>, IAnswerRepository
    {
        public IAdoNetContext _context { get; set; }
        public void InjectContext(IAdoNetContext context)
        {
            _context = context;
        }

        public void InsertAnswer(Answer answer)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.InsertAnswer";

                command.Parameters.Add(new SqlParameter("@QuestionId", answer.QuestionId));
                command.Parameters.Add(new SqlParameter("@Description", answer.Description));
                command.Parameters.Add(new SqlParameter("@CustomerId", answer.CustomerId));

                Execute(command);
            }
        }

        public IEnumerable<Answer> GetAllAnswers()
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetAllAnswers";

                return Execute(command);
            }
        }

        public IEnumerable<Answer> GetAnswerByCustomerId(int customerId)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetAnswerByCustomerId";

                command.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                return Execute(command);
            }
        }

        protected override void Map(IDataRecord record, Answer entity)
        {
            entity.AnswerId = ConvertFromDBVal<int>(record["AnswerId"]);
            entity.QuestionId = ConvertFromDBVal<int>(record["QuestionId"]);
            entity.Description = ConvertFromDBVal<string>(record["Description"]);
            entity.CustomerId = ConvertFromDBVal<int>(record["CustomerId"]);
        }
    }
}

