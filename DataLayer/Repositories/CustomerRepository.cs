﻿using BusinessEntities.Entities;
using DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    /// <summary>
    /// A medium between the data source and the business logic for working with customer entities
    /// </summary>
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public IAdoNetContext _context { get; set; }
        public void InjectContext(IAdoNetContext context)
        {
            _context = context;
        }

        public IEnumerable<Customer> InsertCustomer(Customer customer)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.InsertCustomer";

                command.Parameters.Add(new SqlParameter("@FirstName", customer.FirstName));
                command.Parameters.Add(new SqlParameter("@Surname", customer.Surname));
                command.Parameters.Add(new SqlParameter("@DateOfBirth", customer.DateOfBirth));
                command.Parameters.Add(new SqlParameter("@HomePhoneNumber", customer.HomePhoneNumber));
                command.Parameters.Add(new SqlParameter("@MobilePhoneNumber", customer.MobilePhoneNumber));
                command.Parameters.Add(new SqlParameter("@Email", customer.Email));
                command.Parameters.Add(new SqlParameter("@AddressId", customer.AddressId));
                command.Parameters.Add(new SqlParameter("@Username", customer.Username));
                return Execute(command);
            }
        }

        public IEnumerable<Customer> GetCustomerByCustomerId(int customerId)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetCustomerByCustomerId";

                command.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                return Execute(command);
            }
        }

        public IEnumerable<Customer> GetCustomerBySurname(string surname)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetCustomerBySurname";

                command.Parameters.Add(new SqlParameter("@Surname", surname));

                return Execute(command);
            }
        }

        public IEnumerable<Customer> GetCustomerByEmail(string email)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetCustomerByEmail";

                command.Parameters.Add(new SqlParameter("@Email", email));

                return Execute(command);
            }
        }

        public void DeleteCustomer(int customerId)
        {
            using (var command = _context.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.DeleteCustomer";

                command.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                Execute(command);
            }
        }

        protected override void Map(IDataRecord record, Customer entity)
        {
            entity.CustomerId = ConvertFromDBVal<int>(record["CustomerId"]);
            entity.FirstName = ConvertFromDBVal<string>(record["FirstName"]);
            entity.Surname = ConvertFromDBVal<string>(record["Surname"]);
            entity.DateOfBirth = ConvertFromDBVal<string>(((DateTime)record["DateOfBirth"]).ToString("yyyy-MM-dd"));
            entity.HomePhoneNumber = ConvertFromDBVal<string>(record["HomePhoneNumber"]);
            entity.MobilePhoneNumber = ConvertFromDBVal<string>(record["MobilePhoneNumber"]);
            entity.Email = ConvertFromDBVal<string>(record["Email"]);
            entity.AddressId = ConvertFromDBVal<int>(record["AddressId"]);
            entity.Username = ConvertFromDBVal<string>(record["Username"]);
        }
    }
}
