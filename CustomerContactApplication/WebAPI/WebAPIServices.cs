﻿using CustomerContactApplication.Controllers;
using CustomerContactApplication.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace CustomerContactApplication.WebAPI
{
    /// <summary>
    /// Used to make requests to the Web API
    /// </summary>
    public class WebAPIServices : IWebAPIServices
    {
        private HttpClient _client = new HttpClient();

        public HttpResponseMessage _response { get; set; }
        public string loggedInUser { get; set; }

        public WebAPIServices()
        {
            _client.BaseAddress = new Uri(WebConfigurationManager.AppSettings["WebAPI"]);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

            //Is the user authenticated?
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                var claimsIdentity = System.Threading.Thread.CurrentPrincipal.Identity as ClaimsIdentity;
                loggedInUser = claimsIdentity.FindFirst(ClaimTypes.Name).Value;

                string access_token = claimsIdentity.FindFirst(ClaimTypes.Authentication).Value;
                string token_type = "bearer";

                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token_type, access_token);
            }
        }

        [HttpGet]
        public IEnumerable<T> Get<T>(string controller, string action, HttpRequestBase request, string id = "")
        {
            _response = _client.GetAsync(controller + "/" + action + "/?request=" + id).Result;
            if ((int)_response.StatusCode == 401)
            {
                ForceLogout(request);
            }

            if (_response.IsSuccessStatusCode)
            {
                var result = _response.Content.ReadAsAsync<IEnumerable<T>>().Result;
                return result;
                
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public IEnumerable<T> Post<T>(string controller, string action, HttpRequestBase request, object jsonPostData)
        {
            _response = _client.PostAsJsonAsync(controller + "/" + action + "/", jsonPostData).Result;
            if ((int)_response.StatusCode == 401)
            {
                ForceLogout(request);
            }

            if (_response.IsSuccessStatusCode)
            {
                var result = _response.Content.ReadAsAsync<IEnumerable<T>>().Result;
                return result;
            }
            else
            {
                return null;
            }
        }

        [HttpDelete]
        public IEnumerable<T> Delete<T>(string controller, string action, HttpRequestBase request, string id = "")
        {
            _response = _client.DeleteAsync(controller + "/" + action + "/?request=" + id).Result;
            if ((int)_response.StatusCode == 401)
            {
                ForceLogout(request);
            }

            if (_response.IsSuccessStatusCode)
            {
                var result = _response.Content.ReadAsAsync<IEnumerable<T>>().Result;
                return result;

            }
            else
            {
                return null;
            }
        }

        [HttpPut]
        public IEnumerable<T> Put<T>(string controller, string action, HttpRequestBase request, object jsonPostData)
        {
            //The Put has not yet been implemented
            throw new NotImplementedException();
        }

        /// <summary>
        /// Provides user's credentials to the web api in order to request an authentication token
        /// </summary>
        /// <param name="username">User's username</param>
        /// <param name="password">User's password</param>
        /// <returns></returns>
        [HttpPost]
        public TokenModel RequestAPIToken(string username, string password)
        {
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("UserName", username));
            keyValues.Add(new KeyValuePair<string, string>("Password", password));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));

            _response = _client.PostAsync("token", new FormUrlEncodedContent(keyValues)).Result;
            var result = _response.Content.ReadAsAsync<TokenModel>().Result;

            return result;
        }

        /// <summary>
        /// If at any point we get a 401 from the api - we want to force the user to log out
        /// </summary>
        /// <param name="request"></param>
        public void ForceLogout(HttpRequestBase request)
        {
            var ctx = request.GetOwinContext();
            var authManager = ctx.Authentication;
            authManager.SignOut();
        }

    }
}