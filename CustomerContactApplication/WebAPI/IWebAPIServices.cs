﻿using CustomerContactApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerContactApplication.WebAPI
{
    /// <summary>
    /// List of methods that must be defined by any class that implements this interface
    /// </summary>
    public interface IWebAPIServices
    {
        HttpResponseMessage _response { get; set; }
        string loggedInUser { get; set; }
        IEnumerable<T> Get<T>(string controller, string action, HttpRequestBase request, string id = "");
        IEnumerable<T> Post<T>(string controller, string action, HttpRequestBase request, object jsonPostData);
        IEnumerable<T> Delete<T>(string controller, string action, HttpRequestBase request, string id = "");
        IEnumerable<T> Put<T>(string controller, string action, HttpRequestBase request, object jsonPostData);
        TokenModel RequestAPIToken(string username, string password);
        void ForceLogout(HttpRequestBase request);
    }
}