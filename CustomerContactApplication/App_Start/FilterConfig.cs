﻿using System.Web;
using System.Web.Mvc;

namespace CustomerContactApplication
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            //By default all controllers are not accessible without passing authentication first
            filters.Add(new AuthorizeAttribute());
        }
    }
}
