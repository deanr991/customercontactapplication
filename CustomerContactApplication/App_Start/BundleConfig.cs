﻿using System.Web;
using System.Web.Optimization;

namespace CustomerContactApplication
{
    public class BundleConfig
    {
        /// <summary>
        /// Define script/style bundles for use through out the application
        /// </summary>
        /// <param name="bundles"></param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            bundles.Add(new StyleBundle("~/bundle/site_css").Include("~/Content/Site.css"));
            bundles.Add(new StyleBundle("~/bundle/table_css").Include("~/Content/Table.css"));
            bundles.Add(new StyleBundle("~/bundle/login_css").Include("~/Content/Login.css"));

            bundles.Add(new ScriptBundle("~/bundle/jquery_js").Include("~/Scripts/jquery-2.1.4.min.js",
                                                                       "~/Scripts/jquery.validate.min.js",
                                                                       "~/Scripts/moment.min.js"));

            //We want to enable minification
            BundleTable.EnableOptimizations = true;
        }
    }
}
