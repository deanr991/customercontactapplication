﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CustomerContactApplication.Helpers
{
    /// <summary>
    /// Used to decorate model password properties to ensure that the password is strong enough 
    /// </summary>
    public class PasswordAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                String test = (String)value;

                if (!test.Any(char.IsLetter) || !test.Any(char.IsDigit) || test.Length < 8)
                {
                    return new ValidationResult("Password must be at least 8 characters long and must contain at least one number");
                }

            }
            else
            {
                return new ValidationResult("Password cannot be empty");
            }

            return ValidationResult.Success;
        }
    }
}