﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerContactApplication.Helpers
{
    /// <summary>
    /// Use to convert HttpContent into a string result
    /// </summary>
    public static class HelperMethods
    {
        public static string ContentToString(this HttpContent httpContent)
        {
            var readAsStringAsync = httpContent.ReadAsStringAsync();
            return readAsStringAsync.Result;
        }
    }
}