﻿using CustomerContactApplication.Models;
using CustomerContactApplication.WebAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace CustomerContactApplication.Controllers
{
    /// <summary>
    /// Handle user requests made by unauthenticated users made against the website
    /// </summary>
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private IWebAPIServices _webAPI;
        public AuthController(IWebAPIServices webAPI)
        {
            _webAPI = webAPI;
        }
        /// <summary>
        /// Display the login page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            ModelState.Clear();
            return View("Login");
        }

        /// <summary>
        /// Accept the post request made by the user and send the credentials to the web api for token retrieval
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Login(UserDataModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            var token = _webAPI.RequestAPIToken(model.username, model.password);
            if (token.access_token == null)
            {
                ModelState.Clear();
                ViewBag.FailedLogin = true;
                return View("Login");
            }

            var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.NameIdentifier, token.userId),
                        new Claim(ClaimTypes.Name, model.username),
                        new Claim(ClaimTypes.Authentication, token.access_token),
                        new Claim(ClaimTypes.Expiration, token.expires_in)
                    },"ApplicationCookie");

            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;
            authManager.SignIn(identity);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Error()
        {
            return View("Error");
        }
    }
}