﻿using BusinessEntities.Entities;
using CustomerContactApplication.Helpers;
using CustomerContactApplication.Models;
using CustomerContactApplication.WebAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace CustomerContactApplication.Controllers
{
    /// <summary>
    /// Handle user requests made against the website through the admin panel
    /// </summary>
    public class AdminController : Controller
    {
        private IWebAPIServices _webAPI;

        public AdminController(IWebAPIServices webAPI)
        {
            _webAPI = webAPI;
        }

        /// <summary>
        /// Display the admin panel
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Panel()
        {
            AdminPanelModel model = new AdminPanelModel();

            model = GetStatistics(model);
            return View("Panel", model);
        }

        /// <summary>
        /// Accept the post request made by the user and make a request to the api 
        /// to create a new user using the submitted data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult CreateUser(AdminPanelModel model)
        {
            if (model.Password != model.PasswordAgain)
            {
                model.ErrorMessage = "The passwords do not match";
                return View("Panel", GetStatistics(model));
            }else if (!ModelState.IsValid)
            {
                model.ErrorMessage = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                return View("Panel", GetStatistics(model));
            }

            User newUser = new User();
            newUser.Username = model.Username;
            newUser.Password = model.Password;

            _webAPI.Post<User>("User", "CreateUser", Request, newUser);

            ModelState.Clear();
            model = new AdminPanelModel();

            if (_webAPI._response.IsSuccessStatusCode)
            {
                ViewBag.Success = true;
            }
            else
            {
                model.ErrorMessage = _webAPI._response.Content.ContentToString();
            }

            model = GetStatistics(model);
            return View("Panel", model);
        }

        /// <summary>
        /// Accept the post request made by the user and make a request to the api 
        /// to create a new survey question using the submitted data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult CreateQuestion(AdminPanelModel model)
        {
            Question newQuestion = new Question();
            newQuestion.Description = model.Description;

            _webAPI.Post<Question>("Survey", "CreateQuestion", Request, newQuestion);

            ModelState.Clear();
            model = new AdminPanelModel();

            if (_webAPI._response.IsSuccessStatusCode)
            {
                ViewBag.Success = true;
            }
            else
            {
                model.ErrorMessage = _webAPI._response.Content.ContentToString();
            }

            model = GetStatistics(model);
            return View("Panel", model);
        }

        /// <summary>
        /// Retrieve and display the survey statistics
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public AdminPanelModel GetStatistics(AdminPanelModel model)
        {
            model.QuestionCount = _webAPI.Get<Question>("Survey", "GetAllQuestions", Request).Count();
            model.AnswerCount = _webAPI.Get<Answer>("Survey", "GetAllAnswers", Request).Count();
            return model;
        }

    }
}