﻿using BusinessEntities.Entities;
using CustomerContactApplication.Helpers;
using CustomerContactApplication.Models;
using CustomerContactApplication.WebAPI;
using Newtonsoft.Json.Linq;
using PeerReview.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace CustomerContactApplication.Controllers
{
    /// <summary>
    /// Handle general user requests made against the website
    /// </summary>
    public class HomeController : Controller
    {
        private IWebAPIServices _webAPI;

        public HomeController(IWebAPIServices webAPI)
        {
            _webAPI = webAPI;
        }

        /// <summary>
        /// Display the main page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View("Index");
        }

        /// <summary>
        /// Display the submit data page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Submit()
        {
            SubmitDataModel model = new SubmitDataModel();
            model.SurveyQuestions = GetSurveyQuestions();
            return View(model);
        }

        /// <summary>
        /// Accept the post request made by the user and make a request to the api
        /// to create a new customer using the submitted data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Submit(SubmitDataModel model)
        {
            Customer newCustomer = new Customer();
            newCustomer.FirstName = model.FirstName;
            newCustomer.Surname = model.Surname;
            newCustomer.DateOfBirth = model.DateOfBirth;
            newCustomer.Email = model.Email;
            newCustomer.HomePhoneNumber = model.HomePhoneNumber;
            newCustomer.MobilePhoneNumber = model.MobilePhoneNumber;
            newCustomer.Username = _webAPI.loggedInUser;

            Address newAddress = new Address();
            newAddress.AddressLine1 = model.AddressLine1;
            newAddress.AddressLine2 = model.AddressLine2;
            newAddress.City = model.City;
            newAddress.Postcode = model.Postcode;
            newAddress.Country = model.Country;

            JObject jsonPostData = new JObject();
            jsonPostData["Customer"] = JObject.FromObject(newCustomer);
            jsonPostData["Address"] = JObject.FromObject(newAddress);

            var createdCustomer = _webAPI.Post<Customer>("Customer", "CreateCustomer", Request, jsonPostData);
            if (createdCustomer!= null)
            {
                foreach (var surveyQuestion in model.SurveyQuestions)
                {
                    Answer newAnswer = new Answer();
                    newAnswer.Description = surveyQuestion.answerDescription;
                    newAnswer.QuestionId = surveyQuestion.questionId;
                    newAnswer.CustomerId = createdCustomer.FirstOrDefault().CustomerId;
                    if (newAnswer.Description != "" && newAnswer.Description != null) { 
                        _webAPI.Post<Answer>("Survey", "CreateAnswer", Request, newAnswer);
                    }
                }
            }

            ModelState.Clear();
            model = new SubmitDataModel();

            if (_webAPI._response.IsSuccessStatusCode)
            {
                ViewBag.Success = true;
            }
            else
            {
                model.ErrorMessage = _webAPI._response.Content.ContentToString();
            }

            model.SurveyQuestions = GetSurveyQuestions();

            return View(model);
        }

        /// <summary>
        /// Display the search page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Search()
        {
            return View("Search", new SearchModel());
        }

        [HttpPost]
        public JsonResult SearchBySurname(string searchString)
        {
            var customers = _webAPI.Get<Customer>("Customer", "GetCustomerBySurname", Request, searchString);
            return Json(customers);
        }

        [HttpPost]
        public JsonResult SearchByEmail(string searchString)
        {
            var customers = _webAPI.Get<Customer>("Customer", "GetCustomerByEmail", Request, searchString);
            return Json(customers);
        }

        /// <summary>
        /// Display the page with the customer details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ViewCustomerData(string id)
        {
            CustomerDataModel model = new CustomerDataModel();
            var customer = _webAPI.Get<Customer>("Customer", "GetCustomerByCustomerId", Request, id).FirstOrDefault();
            if (customer != null)
            {
                var address = _webAPI.Get<Address>("Customer", "GetAddressByAddressId", Request, customer.AddressId.ToString()).FirstOrDefault();
                model.Customer = customer;
                model.Address = address;


                var answers = _webAPI.Get<Answer>("Survey", "GetAnswerByCustomerId", Request, id);
                if (answers != null)
                {
                    var questions = _webAPI.Get<Question>("Survey", "GetAllQuestions", Request);
                    foreach (var answer in answers)
                    {
                        model.SurveyQuestions.Add(new QuestionAnswerModel(answer.QuestionId, questions.Where(x=>x.QuestionId == answer.QuestionId).FirstOrDefault().Description, answer.Description));
                    }
                }
            }
            return View(model);
        }

        /// <summary>
        /// Accept the post request made by the user and make a request to the api
        /// to delete the specified customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteCustomer(string id)
        {
            _webAPI.Delete<Customer>("Customer", "DeleteCustomer", Request, id);
            return RedirectToAction("Search", "Home");
        }

        [HttpGet]
        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;
            authManager.SignOut();

            return RedirectToAction("Login", "Auth");
        }

        /// <summary>
        /// Make a request to the web api to retrieve all survey questions
        /// </summary>
        /// <returns></returns>
        public List<QuestionAnswerModel> GetSurveyQuestions()
        {
            List<QuestionAnswerModel> questionList = new List<QuestionAnswerModel>();
            var questions = _webAPI.Get<Question>("Survey", "GetAllQuestions", Request);
            if (questions != null)
            {
                foreach (var question in questions)
                {
                    questionList.Add(new QuestionAnswerModel(question.QuestionId, question.Description));
                }
            }
            return questionList;
        }

    }
}