﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CustomerContactApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }

        /// <summary>
        /// In production we redirect all users to https
        /// </summary>
        protected void Application_BeginRequest()
        {
            if (Context == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (Context.Request.IsSecureConnection)
            {
                return;
            }

            if (string.Equals(Context.Request.Headers["X-Forwarded-Proto"],
                "https",
                StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            if (Context.Request.IsLocal)
            {
                return;
            }

            Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));

        }
    }
}
