﻿using BusinessEntities.Entities;
using CustomerContactApplication.WebAPI;
using PeerReview.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerContactApplication.Models
{
    public class SubmitDataModel
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string DateOfBirth { get; set; }
        public string HomePhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string Email { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public List<QuestionAnswerModel> SurveyQuestions { get; set; } = new List<QuestionAnswerModel>();
        public string ErrorMessage { get; set; }
    }
}