﻿using BusinessEntities.Entities;
using PeerReview.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerContactApplication.Models
{
    public class CustomerDataModel
    {
        public Customer Customer { get; set; }
        public Address Address { get; set; }
        public List<QuestionAnswerModel> SurveyQuestions { get; set; } = new List<QuestionAnswerModel>();
    }
}