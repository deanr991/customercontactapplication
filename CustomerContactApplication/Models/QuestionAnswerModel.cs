﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeerReview.Models
{
    public class QuestionAnswerModel
    {
        public int? questionId { get; set; }
        public string questionDescription { get; set; }
        public string answerDescription { get; set; } = null;

        public QuestionAnswerModel() { }

        public QuestionAnswerModel(int? questionId, string questionDescription)
        {
            this.questionId = questionId;
            this.questionDescription = questionDescription;
        }

        public QuestionAnswerModel(int? questionId, string questionDescription, string answerDescription)
        {
            this.questionId = questionId;
            this.questionDescription = questionDescription;
            this.answerDescription = answerDescription;
        }

    }
}