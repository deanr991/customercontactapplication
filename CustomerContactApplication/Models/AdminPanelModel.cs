﻿using CustomerContactApplication.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CustomerContactApplication.Models
{
    public class AdminPanelModel
    {
        [StringLength(100)]
        public string Username { get; set; }

        [Password]
        [StringLength(500)]
        public string Password { get; set; }

        [Password]
        [StringLength(500)]
        public string PasswordAgain { get; set; }

        public string Salt { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public int QuestionCount { get; set; }
        public int AnswerCount { get; set; }

        public string ErrorMessage { get; set; }
    }
}