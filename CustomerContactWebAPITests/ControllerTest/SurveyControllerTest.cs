﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web.Http;
using BusinessServices.Services;
using System.Collections.Generic;
using BusinessEntities.Entities;
using CustomerContactWebAPI.Controllers;
using System.Linq;
using System.Diagnostics;

namespace CustomerContactWebAPITests.ControllerTest
{
    [TestClass]
    public class SurveyControllerTest
    {
        [TestMethod]
        public void TestGetAllQuestions3Results()
        {
            //Arrange
            Mock<ISurveyServices> servicesMock = new Mock<ISurveyServices>();

            List<Question> list = new List<Question>();
            list.Add(new Question());
            list.Add(new Question());
            list.Add(new Question());
            IEnumerable<Question> returnResults = list;

            servicesMock.Setup(p => p.GetAllQuestions()).Returns(returnResults);

            //Act
            var controller = new SurveyController(servicesMock.Object);
            var questions = controller.GetAllQuestions();

            //Assert
            Assert.AreEqual(3, questions.Count());
        }

        [TestMethod]
        public void TestGetAllQuestionsNoResults()
        {
            //Arrange
            Mock<ISurveyServices> servicesMock = new Mock<ISurveyServices>();

            List<Question> list = new List<Question>();
            IEnumerable<Question> returnResults = list;

            servicesMock.Setup(p => p.GetAllQuestions()).Returns(returnResults);

            //Act
            var controller = new SurveyController(servicesMock.Object);
            var questions = controller.GetAllQuestions();

            //Assert
            Assert.AreEqual(0, questions.Count());
        }

        [TestMethod]
        public void TestGetQuestionByQuestionId()
        {
            //Arrange
            Mock<ISurveyServices> servicesMock = new Mock<ISurveyServices>();

            int questionId = 999;

            List<Question> list = new List<Question>();
            list.Add(new Question() { QuestionId = questionId });
            IEnumerable<Question> returnResults = list;

            servicesMock.Setup(p => p.GetQuestionByQuestionId(questionId)).Returns(returnResults);

            //Act
            var controller = new SurveyController(servicesMock.Object);
            var question = controller.GetQuestionByQuestionId(questionId);

            //Assert
            Assert.AreEqual(1, question.Count());
            Assert.AreEqual(999, question.FirstOrDefault().QuestionId);
        }

        [TestMethod]
        public void TestGetAllAnswers3Results()
        {
            //Arrange
            Mock<ISurveyServices> servicesMock = new Mock<ISurveyServices>();

            List<Answer> list = new List<Answer>();
            list.Add(new Answer());
            list.Add(new Answer());
            list.Add(new Answer());
            IEnumerable<Answer> returnResults = list;

            servicesMock.Setup(p => p.GetAllAnswers()).Returns(returnResults);

            //Act
            var controller = new SurveyController(servicesMock.Object);
            var answers = controller.GetAllAnswers();

            //Assert
            Assert.AreEqual(3, answers.Count());
        }

        [TestMethod]
        public void TestGetAllAnswersNoResults()
        {
            //Arrange
            Mock<ISurveyServices> servicesMock = new Mock<ISurveyServices>();

            List<Answer> list = new List<Answer>();
            IEnumerable<Answer> returnResults = list;

            servicesMock.Setup(p => p.GetAllAnswers()).Returns(returnResults);

            //Act
            var controller = new SurveyController(servicesMock.Object);
            var answers = controller.GetAllAnswers();

            //Assert
            Assert.AreEqual(0, answers.Count());
        }

    }
}
