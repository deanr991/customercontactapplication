﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web.Http;
using BusinessServices.Services;
using System.Collections.Generic;
using BusinessEntities.Entities;
using CustomerContactWebAPI.Controllers;
using System.Linq;
using System.Diagnostics;

namespace CustomerContactWebAPITests
{
    [TestClass]
    public class CustomerControllerTest
    {
        [TestMethod]
        public void TestGetCustomerBySurname3Results()
        {
            //Arrange
            Mock<ICustomerServices> servicesMock = new Mock<ICustomerServices>();

            string surname = "test";
            List<Customer> list = new List<Customer>();
            list.Add(new Customer());
            list.Add(new Customer());
            list.Add(new Customer());
            IEnumerable<Customer> returnResults = list;

            servicesMock.Setup(p => p.GetCustomerBySurname(surname)).Returns(returnResults);

            //Act
            var controller = new CustomerController(servicesMock.Object);
            var customers = controller.GetCustomerBySurname(surname);

            //Assert
            Assert.AreEqual(3, customers.Count());
        }

        [TestMethod]
        public void TestGetCustomerBySurnameNoResults()
        {
            //Arrange
            Mock<ICustomerServices> servicesMock = new Mock<ICustomerServices>();

            string surname = "test";
            List<Customer> list = new List<Customer>();
            IEnumerable<Customer> returnResults = list;

            servicesMock.Setup(p => p.GetCustomerBySurname(surname)).Returns(returnResults);

            //Act
            var controller = new CustomerController(servicesMock.Object);
            var customers = controller.GetCustomerBySurname(surname);

            //Assert
            Assert.AreEqual(0, customers.Count());
        }

        [TestMethod]
        public void TestGetCustomerByCustomerId()
        {
            //Arrange
            Mock<ICustomerServices> servicesMock = new Mock<ICustomerServices>();

            int customerId = 999;

            List<Customer> list = new List<Customer>();
            list.Add(new Customer() { CustomerId = customerId });
            IEnumerable<Customer> returnResults = list;

            servicesMock.Setup(p => p.GetCustomerByCustomerId(customerId)).Returns(returnResults);

            //Act
            var controller = new CustomerController(servicesMock.Object);
            var customer = controller.GetCustomerByCustomerId(customerId);

            //Assert
            Assert.AreEqual(1, customer.Count());
            Assert.AreEqual(999, customer.FirstOrDefault().CustomerId);
        }

        [TestMethod]
        public void TestGetCustomerByCustomerEmail()
        {
            //Arrange
            Mock<ICustomerServices> servicesMock = new Mock<ICustomerServices>();

            string customerEmail = "test@test.com";

            List<Customer> list = new List<Customer>();
            list.Add(new Customer() { Email = customerEmail });
            IEnumerable<Customer> returnResults = list;

            servicesMock.Setup(p => p.GetCustomerByEmail(customerEmail)).Returns(returnResults);

            //Act
            var controller = new CustomerController(servicesMock.Object);
            var customer = controller.GetCustomerByEmail(customerEmail);

            //Assert
            Assert.AreEqual(1, customer.Count());
            Assert.AreEqual("test@test.com", customer.FirstOrDefault().Email);
        }
    }
}
