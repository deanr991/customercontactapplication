﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web.Http;
using BusinessServices.Services;
using System.Collections.Generic;
using BusinessEntities.Entities;
using CustomerContactWebAPI.Controllers;
using System.Linq;
using System.Diagnostics;

namespace CustomerContactWebAPITests.ControllerTest
{
    [TestClass]
    public class UserControllerTest
    {
        [TestMethod]
        public void TestGetUserByUserId()
        {
            //Arrange
            Mock<IUserServices> servicesMock = new Mock<IUserServices>();

            int userId = 999;

            List<User> list = new List<User>();
            list.Add(new User() { UserId = userId });
            IEnumerable<User> returnResults = list;

            servicesMock.Setup(p => p.GetUserByUserId(userId)).Returns(returnResults);

            //Act
            var controller = new UserController(servicesMock.Object);
            var user = controller.GetUserByUserId(userId);

            //Assert
            Assert.AreEqual(1, user.Count());
            Assert.AreEqual(999, user.FirstOrDefault().UserId);
        }
    }
}
