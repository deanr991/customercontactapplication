﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BusinessServices.Services;
using BusinessEntities.Entities;
using System.Linq;
using DataLayer.Interfaces;
using CustomerContactWebAPITests.Helpers;

namespace CustomerContactWebAPITests.ServicesTest
{
    [TestClass]
    public class UserServicesTest
    {
        [TestMethod]
        [ExpectedException(typeof(UserException))]
        public void CreateUserAlreadyExists()
        {
            //Arrange
            Mock<IAdoNetContext> contextMock = new Mock<IAdoNetContext>();
            Mock<IAdoNetUnitOfWork> uowMock = new Mock<IAdoNetUnitOfWork>();
            Mock<IUserRepository> userRepositoryMock = new Mock<IUserRepository>();

            User newUser = new User();
            newUser.Username = "test";
            newUser.Password = "password";

            uowMock.Setup(p => p.SaveChanges()).Verifiable();
            contextMock.Setup(p => p.CreateUnitOfWork()).Returns(uowMock.Object).Verifiable();
            userRepositoryMock.Setup(p => p.GetUserByUsername(newUser.Username)).Returns(new User().ToEnumerable()).Verifiable();

            //Act
            var service = new UserServices(contextMock.Object, userRepositoryMock.Object, uowMock.Object);
            service.CreateUser(newUser);

            //Assert: This test must throw an exception to pass (see attribute)
        }

        public void TestGetUserByUserIdNoResults()
        {
            //Arrange
            Mock<IAdoNetContext> contextMock = new Mock<IAdoNetContext>();
            Mock<IAdoNetUnitOfWork> uowMock = new Mock<IAdoNetUnitOfWork>();
            Mock<IUserRepository> userRepositoryMock = new Mock<IUserRepository>();

            int userId = 999;

            uowMock.Setup(p => p.SaveChanges()).Verifiable();
            contextMock.Setup(p => p.CreateUnitOfWork()).Returns(uowMock.Object).Verifiable();
            userRepositoryMock.Setup(p => p.GetUserByUserId(userId)).Returns(Enumerable.Empty<User>()).Verifiable();

            //Act
            var service = new UserServices(contextMock.Object, userRepositoryMock.Object, uowMock.Object);
            var validatedUser = service.GetUserByUserId(userId);

            //Assert
            Assert.AreEqual(0, validatedUser.Count());
        }
    }
}
