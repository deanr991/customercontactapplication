﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BusinessServices.Services;
using BusinessEntities.Entities;
using System.Linq;
using DataLayer.Interfaces;
using CustomerContactWebAPITests.Helpers;

namespace CustomerContactWebAPITests.ServicesTest
{
    [TestClass]
    public class CustomerServicesTest
    {
        [TestMethod]
        [ExpectedException(typeof(CustomerException))]
        public void TestCreateCustomerEmailAlreadyExists()
        {
            //Arrange
            Mock<IAdoNetContext> contextMock = new Mock<IAdoNetContext>();
            Mock<IAdoNetUnitOfWork> uowMock = new Mock<IAdoNetUnitOfWork>();
            Mock<ICustomerRepository> customerRepositoryMock = new Mock<ICustomerRepository>();
            Mock<IAddressRepository> addressRepositoryMock = new Mock<IAddressRepository>();

            Customer newCustomer = new Customer();
            newCustomer.FirstName = "test";
            newCustomer.Surname = "test";
            newCustomer.DateOfBirth = "01-01-1991";
            newCustomer.Email = "test@gmail.com";
            newCustomer.HomePhoneNumber = "999999999";
            newCustomer.MobilePhoneNumber = "999999999";
            newCustomer.Username = "test"; 

            Address newAddress = new Address();
            newAddress.AddressLine1 = "test"; 
            newAddress.AddressLine2 = "test";
            newAddress.City = "test";
            newAddress.Postcode = "test";
            newAddress.Country = "test";

            Address createdAddressMock = new Address();
            createdAddressMock.AddressId = 999;
            createdAddressMock.AddressLine1 = "test";
            createdAddressMock.AddressLine2 = "test";
            createdAddressMock.City = "test";
            createdAddressMock.Postcode = "test";
            createdAddressMock.Country = "test";         
            addressRepositoryMock.Setup(p => p.InsertAddress(newAddress)).Returns(createdAddressMock.ToEnumerable()).Verifiable();

            Customer createdCustomerMock = new Customer();
            createdCustomerMock.FirstName = "test";
            createdCustomerMock.Surname = "test";
            createdCustomerMock.DateOfBirth = "01-01-1991";
            createdCustomerMock.Email = "test@gmail.com";
            createdCustomerMock.HomePhoneNumber = "999999999";
            createdCustomerMock.MobilePhoneNumber = "999999999";
            createdCustomerMock.Username = "test";
            customerRepositoryMock.Setup(p => p.InsertCustomer(newCustomer)).Returns(createdCustomerMock.ToEnumerable()).Verifiable();

            uowMock.Setup(p => p.SaveChanges()).Verifiable();
            contextMock.Setup(p => p.CreateUnitOfWork()).Returns(uowMock.Object);
            customerRepositoryMock.Setup(p => p.GetCustomerByEmail(newCustomer.Email)).Returns(new Customer().ToEnumerable()).Verifiable();

            //Act
            var service = new CustomerServices(contextMock.Object, customerRepositoryMock.Object, addressRepositoryMock.Object, uowMock.Object);
            var createdCustomer = service.CreateCustomer(newCustomer, newAddress);

            //Assert: This test must throw an exception to pass (see attribute)
        }

        [TestMethod]
        public void TestCreateCustomerSuccessful()
        {
            //Arrange
            Mock<IAdoNetContext> contextMock = new Mock<IAdoNetContext>();
            Mock<IAdoNetUnitOfWork> uowMock = new Mock<IAdoNetUnitOfWork>();
            Mock<ICustomerRepository> customerRepositoryMock = new Mock<ICustomerRepository>();
            Mock<IAddressRepository> addressRepositoryMock = new Mock<IAddressRepository>();

            Customer newCustomer = new Customer();
            newCustomer.FirstName = "test";
            newCustomer.Surname = "test";
            newCustomer.DateOfBirth = "01-01-1991";
            newCustomer.Email = "test@gmail.com";
            newCustomer.HomePhoneNumber = "999999999";
            newCustomer.MobilePhoneNumber = "999999999";
            newCustomer.Username = "test";

            Address newAddress = new Address();
            newAddress.AddressLine1 = "test";
            newAddress.AddressLine2 = "test";
            newAddress.City = "test";
            newAddress.Postcode = "test";
            newAddress.Country = "test";

            Address createdAddressMock = new Address();
            createdAddressMock.AddressId = 999;
            createdAddressMock.AddressLine1 = "test";
            createdAddressMock.AddressLine2 = "test";
            createdAddressMock.City = "test";
            createdAddressMock.Postcode = "test";
            createdAddressMock.Country = "test";
            addressRepositoryMock.Setup(p => p.InsertAddress(newAddress)).Returns(createdAddressMock.ToEnumerable()).Verifiable();

            Customer createdCustomerMock = new Customer();
            createdCustomerMock.FirstName = "test";
            createdCustomerMock.Surname = "test";
            createdCustomerMock.DateOfBirth = "01-01-1991";
            createdCustomerMock.Email = "test@gmail.com";
            createdCustomerMock.HomePhoneNumber = "999999999";
            createdCustomerMock.MobilePhoneNumber = "999999999";
            createdCustomerMock.Username = "test";
            createdCustomerMock.AddressId = 999;
            customerRepositoryMock.Setup(p => p.InsertCustomer(newCustomer)).Returns(createdCustomerMock.ToEnumerable()).Verifiable();

            uowMock.Setup(p => p.SaveChanges()).Verifiable();
            contextMock.Setup(p => p.CreateUnitOfWork()).Returns(uowMock.Object);
            customerRepositoryMock.Setup(p => p.GetCustomerByEmail(newCustomer.Email)).Returns(Enumerable.Empty<Customer>()).Verifiable();

            //Act
            var service = new CustomerServices(contextMock.Object, customerRepositoryMock.Object, addressRepositoryMock.Object, uowMock.Object);
            var createdCustomer = service.CreateCustomer(newCustomer, newAddress);

            //Assert
            Assert.AreEqual(1, createdCustomer.Count());
            Assert.AreEqual(999, createdCustomer.FirstOrDefault().AddressId);
        }

        [TestMethod]
        public void TestGetCustomerByCustomerIdNoResults()
        {
            //Arrange
            Mock<IAdoNetContext> contextMock = new Mock<IAdoNetContext>();
            Mock<IAdoNetUnitOfWork> uowMock = new Mock<IAdoNetUnitOfWork>();
            Mock<ICustomerRepository> customerRepositoryMock = new Mock<ICustomerRepository>();
            Mock<IAddressRepository> addressRepositoryMock = new Mock<IAddressRepository>();

            int customerId = 999;

            uowMock.Setup(p => p.SaveChanges()).Verifiable();
            contextMock.Setup(p => p.CreateUnitOfWork()).Returns(uowMock.Object);
            customerRepositoryMock.Setup(p => p.GetCustomerByCustomerId(customerId)).Returns(Enumerable.Empty<Customer>()).Verifiable();

            //Act
            var service = new CustomerServices(contextMock.Object, customerRepositoryMock.Object, addressRepositoryMock.Object, uowMock.Object);
            var customer = service.GetCustomerByCustomerId(customerId);

            //Assert
            Assert.AreEqual(0, customer.Count());
        }
    }
}
