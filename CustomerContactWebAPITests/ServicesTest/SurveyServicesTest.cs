﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BusinessServices.Services;
using BusinessEntities.Entities;
using System.Linq;
using DataLayer.Interfaces;
using CustomerContactWebAPITests.Helpers;
using System.Collections.Generic;

namespace CustomerContactWebAPITests.ServicesTest
{
    [TestClass]
    public class SurveyServicesTest
    {
        [TestMethod]
        public void TestGetAnswerByCustomerId4Results()
        {
            //Arrange
            Mock<IAdoNetContext> contextMock = new Mock<IAdoNetContext>();
            Mock<IAdoNetUnitOfWork> uowMock = new Mock<IAdoNetUnitOfWork>();
            Mock<IAnswerRepository> answerRepositoryMock = new Mock<IAnswerRepository>();
            Mock<IQuestionRepository> questionRepositoryMock = new Mock<IQuestionRepository>();

            uowMock.Setup(p => p.SaveChanges()).Verifiable();
            contextMock.Setup(p => p.CreateUnitOfWork()).Returns(uowMock.Object).Verifiable();

            int customerId = 999;
            List<Answer> list = new List<Answer>();
            list.Add(new Answer());
            list.Add(new Answer());
            list.Add(new Answer());
            list.Add(new Answer());
            IEnumerable<Answer> returnResults = list;

            answerRepositoryMock.Setup(p => p.GetAnswerByCustomerId(customerId)).Returns(returnResults);

            //Act
            var service = new SurveyServices(contextMock.Object, questionRepositoryMock.Object, answerRepositoryMock.Object, uowMock.Object);
            var answers = service.GetAnswerByCustomerId(customerId);

            //Assert
            Assert.AreEqual(4, answers.Count());
        }

        [TestMethod]
        public void TestGetAnswerByCustomerIdNoResults()
        {
            //Arrange
            Mock<IAdoNetContext> contextMock = new Mock<IAdoNetContext>();
            Mock<IAdoNetUnitOfWork> uowMock = new Mock<IAdoNetUnitOfWork>();
            Mock<IAnswerRepository> answerRepositoryMock = new Mock<IAnswerRepository>();
            Mock<IQuestionRepository> questionRepositoryMock = new Mock<IQuestionRepository>();

            uowMock.Setup(p => p.SaveChanges()).Verifiable();
            contextMock.Setup(p => p.CreateUnitOfWork()).Returns(uowMock.Object).Verifiable();

            int customerId = 999;

            answerRepositoryMock.Setup(p => p.GetAnswerByCustomerId(customerId)).Returns(Enumerable.Empty<Answer>());

            //Act
            var service = new SurveyServices(contextMock.Object, questionRepositoryMock.Object, answerRepositoryMock.Object, uowMock.Object);
            var answers = service.GetAnswerByCustomerId(customerId);

            //Assert
            Assert.AreEqual(0, answers.Count());
        }
    }
}
