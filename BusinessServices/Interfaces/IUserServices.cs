﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;

namespace BusinessServices.Services
{
    /// <summary>
    /// List of methods that must be defined by any class that implements this interface
    /// </summary>
    public interface IUserServices:IDisposable
    {
        IEnumerable<User> GetUserByUserId(int userId);
        IEnumerable<User> GetValidatedUser(string username, string password);
        void CreateUser(User user);
    }
}
