﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessServices.Interfaces
{
    /// <summary>
    /// List of methods that must be defined by any class that implements this interface
    /// </summary>
    public interface ILoggingServices : IDisposable
    {
        void InsertError(ErrorLog error);
    }
}
