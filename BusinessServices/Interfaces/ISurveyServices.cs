﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;

namespace BusinessServices.Services
{
    /// <summary>
    /// List of methods that must be defined by any class that implements this interface
    /// </summary>
    public interface ISurveyServices : IDisposable
    {
        IEnumerable<Question> GetQuestionByQuestionId(int questionId);
        IEnumerable<Question> GetAllQuestions();
        IEnumerable<Answer> GetAllAnswers();
        void CreateAnswer(Answer answer);
        void CreateQuestion(Question question);
        IEnumerable<Answer> GetAnswerByCustomerId(int customerId);
    }
}
