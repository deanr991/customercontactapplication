﻿using BusinessEntities.Entities;
using System;
using System.Collections.Generic;

namespace BusinessServices.Services
{
    /// <summary>
    /// List of methods that must be defined by any class that implements this interface
    /// </summary>
    public interface ICustomerServices : IDisposable
    {
        IEnumerable<Customer> GetCustomerByCustomerId(int customerId);
        IEnumerable<Customer> GetCustomerBySurname(string surname);
        IEnumerable<Customer> GetCustomerByEmail(string email);
        IEnumerable<Customer> CreateCustomer(Customer customer, Address address);
        IEnumerable<Address> GetAddressByAddressId(int addressId);
        void DeleteCustomer(int customerId);
    }
}
