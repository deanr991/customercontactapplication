﻿using System;

/// <summary>
/// Collection of Exception classes which are used by the business layer to raise custom exceptions
/// </summary>
namespace BusinessServices.Services
{
    public class CustomerException : Exception
    {
        public CustomerException(string message) : base(message) { }
    }

    public class UserException : Exception
    {
        public UserException(string message) : base(message) { }
    }
}