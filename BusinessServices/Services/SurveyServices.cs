﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities.Entities;
using DataLayer.Repositories;
using DataLayer;
using DataLayer.Interfaces;

namespace BusinessServices.Services
{
    /// <summary>
    /// Survey business logic used to access the question/answer repositories within the data layer
    /// </summary>
    public class SurveyServices : ISurveyServices
    {
        private readonly IAdoNetContext _context;
        private readonly IQuestionRepository _questionRepository;
        private readonly IAnswerRepository _answerRepository;
        private IAdoNetUnitOfWork _uow;

        public SurveyServices(IAdoNetContext context, IQuestionRepository questionRepository, 
            IAnswerRepository answerRepository, IAdoNetUnitOfWork uow)
        {
            _context = context;
            _uow = uow;
            _questionRepository = questionRepository;
            _questionRepository.InjectContext(_context);
            _answerRepository = answerRepository;
            _answerRepository.InjectContext(_context);
        }
        public void CreateAnswer(Answer answer)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                _answerRepository.InsertAnswer(answer);
                _uow.SaveChanges();
            }
        }

        public void CreateQuestion(Question question)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                _questionRepository.InsertQuestion(question);
                _uow.SaveChanges();
            }
        }

        public IEnumerable<Question> GetQuestionByQuestionId(int questionId)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var question = _questionRepository.GetQuestionByQuestionId(questionId);
                _uow.SaveChanges();
                return question;
            }
        }

        public IEnumerable<Question> GetAllQuestions()
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var questions = _questionRepository.GetAllQuestions();
                _uow.SaveChanges();
                return questions;
            }
        }

        public IEnumerable<Answer> GetAllAnswers()
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var answers = _answerRepository.GetAllAnswers();
                _uow.SaveChanges();
                return answers;
            }
        }

        public IEnumerable<Answer> GetAnswerByCustomerId(int customerId)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var answer = _answerRepository.GetAnswerByCustomerId(customerId);
                _uow.SaveChanges();
                return answer;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
