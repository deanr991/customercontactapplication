﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities.Entities;
using DataLayer;
using DataLayer.Repositories;
using DataLayer.Interfaces;

namespace BusinessServices.Services
{
    /// <summary>
    /// Customer business logic used to access the address/customer repositories within the data layer
    /// </summary>
    public class CustomerServices : ICustomerServices
    {
        private readonly IAdoNetContext _context;
        private readonly ICustomerRepository _customerRepository;
        private readonly IAddressRepository _addressRepository;
        private IAdoNetUnitOfWork _uow;

        public CustomerServices(IAdoNetContext context, ICustomerRepository customerRepository, 
            IAddressRepository addressRepository, IAdoNetUnitOfWork uow)
        {
            _context = context;
            _uow = uow;
            _customerRepository = customerRepository;
            _customerRepository.InjectContext(_context);
            _addressRepository = addressRepository;
            _addressRepository.InjectContext(_context);
        }
        public IEnumerable<Customer> CreateCustomer(Customer customer, Address address)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                if (_customerRepository.GetCustomerByEmail(customer.Email).Any())
                {
                    throw new CustomerException("A customer with this email already exists");
                }

                var addressId = _addressRepository.InsertAddress(address).FirstOrDefault().AddressId;
                customer.AddressId = addressId;
                var createdCustomer = _customerRepository.InsertCustomer(customer);
                _uow.SaveChanges();
                return createdCustomer;
            }
        }

        public IEnumerable<Customer> GetCustomerByCustomerId(int customerId)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var customer = _customerRepository.GetCustomerByCustomerId(customerId);
                _uow.SaveChanges();
                return customer;
            }
        }

        public IEnumerable<Customer> GetCustomerBySurname(string surname)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var customer = _customerRepository.GetCustomerBySurname(surname);
                _uow.SaveChanges();
                return customer;
            }
        }
        public IEnumerable<Customer> GetCustomerByEmail(string email)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var customer = _customerRepository.GetCustomerByEmail(email);
                _uow.SaveChanges();
                return customer;
            }
        }

        public IEnumerable<Address> GetAddressByAddressId(int addressId)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var address = _addressRepository.GetAddressByAddressId(addressId);
                _uow.SaveChanges();
                return address;
            }
        }

        public void DeleteCustomer(int customerId)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                _customerRepository.DeleteCustomer(customerId);
                _uow.SaveChanges();
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
