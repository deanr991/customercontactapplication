﻿using BusinessServices.Interfaces;
using DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities.Entities;

namespace BusinessServices.Services
{
    /// <summary>
    /// Logging business logic used to access the ErrorLog repository within the data layer
    /// </summary>
    public class LoggingServices : ILoggingServices
    {
        private readonly IAdoNetContext _context;
        private readonly IErrorLogRepository _errorLogRepository;
        private IAdoNetUnitOfWork _uow;

        public LoggingServices(IAdoNetContext context, IErrorLogRepository errorLogRepository, IAdoNetUnitOfWork uow)
        {
            _context = context;
            _uow = uow;
            _errorLogRepository = errorLogRepository;
            _errorLogRepository.InjectContext(_context);
        }

        public void InsertError(ErrorLog error)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                _errorLogRepository.InsertErrorLogEntry(error);
                _uow.SaveChanges();
            }

        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
