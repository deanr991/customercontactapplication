﻿using BusinessEntities.Entities;
using DataLayer;
using DataLayer.Interfaces;
using DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BusinessServices.Services
{
    /// <summary>
    /// User business logic used to access the user repository within the data layer
    /// </summary>
    public class UserServices:IUserServices
    {
        private readonly IAdoNetContext _context;
        private readonly IUserRepository _userRepository;
        private IAdoNetUnitOfWork _uow;

        public UserServices(IAdoNetContext context, IUserRepository userRepository, IAdoNetUnitOfWork uow)
        {
            _context = context;
            _uow = uow;
            _userRepository = userRepository;
            _userRepository.InjectContext(_context);
        }

        public void CreateUser(User user)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                if (_userRepository.GetUserByUsername(user.Username).Any())
                {
                    throw new UserException("User '" + user.Username + "' already exists");
                }

                string salt = GenerateRandomString();
                user.Password = Hash(user.Password, salt);
                user.Salt = salt;
                _userRepository.InsertUser(user);
                _uow.SaveChanges();
            }
        }

        public IEnumerable<User> GetUserByUserId(int userId)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var user = _userRepository.GetUserByUserId(userId);
                _uow.SaveChanges();
                return user;
            }
        }

        public IEnumerable<User> GetValidatedUser(string username, string password)
        {
            using (_uow = _context.CreateUnitOfWork())
            {
                var requestedUser = _userRepository.GetUserByUsername(username).FirstOrDefault();
                if (requestedUser == null)
                {
                    return Enumerable.Empty<User>();
                }

                string userSalt = requestedUser.Salt;
                string hashedPassword = Hash(password, userSalt);

                var validatedUser = _userRepository.GetValidatedUser(username, hashedPassword);
                _uow.SaveChanges();
                return validatedUser;
            }
        }

        private string Hash(string password, string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);
            byte[] src = Encoding.Unicode.GetBytes(salt);
            byte[] dst = new byte[src.Length + bytes.Length];
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
            HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
            byte[] inarray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inarray);
        }

        private string GenerateRandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random(Guid.NewGuid().GetHashCode());
            return new string(Enumerable.Repeat(chars, 32)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
