﻿CREATE TABLE [dbo].[Question]
(
	[QuestionId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Description] NVARCHAR(500) NOT NULL
)
