﻿CREATE TABLE [dbo].[User]
(
	[UserId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Username] NVARCHAR(100) NOT NULL, 
    [Password] NVARCHAR(500) NOT NULL, 
    [Salt] NVARCHAR(500) NOT NULL,
	CONSTRAINT [UQ_Username] UNIQUE NONCLUSTERED ([Username])
)
GO
CREATE INDEX Username_Index
ON [User] (Username)