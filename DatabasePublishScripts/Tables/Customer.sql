﻿CREATE TABLE [dbo].[Customer]
(
	[CustomerId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FirstName] NVARCHAR(500) NULL, 
    [Surname] NVARCHAR(500) NULL, 
    [DateOfBirth] DATE NULL, 
    [HomePhoneNumber] NVARCHAR(500) NULL, 
    [MobilePhoneNumber] NVARCHAR(500) NULL, 
    [Email] NVARCHAR(500) NULL, 
    [AddressId] INT NOT NULL, 
    [Username] NVARCHAR(100) NOT NULL, 
    CONSTRAINT [FK_Customer_ToUser] FOREIGN KEY ([Username]) REFERENCES [User]([Username]), 
    CONSTRAINT [FK_Customer_ToAddress] FOREIGN KEY ([AddressId]) REFERENCES [Address]([AddressId]),
	CONSTRAINT [UQ_Email] UNIQUE NONCLUSTERED ([Email])
)
GO
CREATE INDEX Surname_Index
ON Customer (Surname)
GO
CREATE INDEX Email_Index
ON Customer (Email)