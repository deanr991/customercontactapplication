﻿CREATE TABLE [dbo].[ErrorLog]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Source] NVARCHAR(MAX) NULL, 
    [Method] NVARCHAR(MAX) NULL, 
    [RequestUri] NVARCHAR(MAX) NULL, 
    [Exception] NVARCHAR(MAX) NULL
)
