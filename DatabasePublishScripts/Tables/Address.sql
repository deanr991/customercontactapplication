﻿CREATE TABLE [dbo].[Address]
(
	[AddressId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AddressLine1] NVARCHAR(500) NULL, 
    [AddressLine2] NVARCHAR(500) NULL, 
    [City] NVARCHAR(500) NULL, 
    [Postcode] NVARCHAR(500) NULL, 
    [Country] NVARCHAR(500) NULL
)
