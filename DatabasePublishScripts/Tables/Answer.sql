﻿CREATE TABLE [dbo].[Answer]
(
	[AnswerId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [QuestionId] INT NOT NULL, 
    [Description] NVARCHAR(500) NULL, 
    [CustomerId] INT NOT NULL, 
    CONSTRAINT [FK_Answer_ToQuestion] FOREIGN KEY ([QuestionId]) REFERENCES [Question]([QuestionId]), 
    CONSTRAINT [FK_Answer_ToCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [Customer]([CustomerId])
)
GO
CREATE INDEX CustomerId_Index
ON Answer (CustomerId)