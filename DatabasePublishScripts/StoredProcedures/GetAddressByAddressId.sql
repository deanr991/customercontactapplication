﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve address information by
--				AddressId
-- =============================================

CREATE PROCEDURE [dbo].[GetAddressByAddressId]
	@AddressId INT
AS
	SELECT 
		AddressId, 
		AddressLine1, 
		AddressLine2, 
		City, 
		Postcode, 
		Country
	FROM
		[dbo].[Address] a
	WHERE 
		a.AddressId = @AddressId;