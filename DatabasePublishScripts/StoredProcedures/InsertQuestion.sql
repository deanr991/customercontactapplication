﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Insert survey question
-- =============================================

CREATE PROCEDURE [dbo].[InsertQuestion]
	@Description NVARCHAR(500)
AS
	INSERT INTO
		[dbo].[Question]
	VALUES(
		@Description
	);

