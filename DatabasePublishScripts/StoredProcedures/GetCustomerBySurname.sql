﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve customer information by
--				Surname
-- =============================================

CREATE PROCEDURE [dbo].[GetCustomerBySurname]
	@Surname NVARCHAR(500)
AS
	SELECT 
		CustomerId,
		FirstName,
		Surname,
		DateOfBirth,
		HomePhoneNumber,
		MobilePhoneNumber,
		Email,
		AddressId,
		Username
	FROM
		[dbo].[Customer] c
	WHERE 
		c.Surname LIKE '%'+@Surname+'%';

