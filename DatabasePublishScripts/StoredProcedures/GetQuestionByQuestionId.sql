﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve question information by
--				QuestionId
-- =============================================

CREATE PROCEDURE [dbo].[GetQuestionByQuestionId]
	@QuestionId INT
AS
	SELECT 
		QuestionId,
		Description
	FROM
		[dbo].[Question] q
	WHERE 
		q.QuestionId = @QuestionId;

