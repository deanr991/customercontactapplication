﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Insert Answer
-- =============================================

CREATE PROCEDURE [dbo].[InsertAnswer]
	@QuestionId INT,
	@Description NVARCHAR(500),
	@CustomerId INT
AS
	INSERT INTO
		[dbo].[Answer]
	VALUES(
		@QuestionId,
		@Description,
		@CustomerId
	);

