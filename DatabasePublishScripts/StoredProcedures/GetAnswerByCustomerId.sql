﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve customer survey answers
--				by CustomerId
-- =============================================

CREATE PROCEDURE [dbo].[GetAnswerByCustomerId]
	@CustomerId INT
AS
	SELECT 
		AnswerId,
		QuestionId,
		Description,
		CustomerId
	FROM
		[dbo].[Answer] a
	WHERE 
		a.CustomerId = @CustomerId;
