﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve all answers from the
--				database
-- =============================================

CREATE PROCEDURE [dbo].[GetAllAnswers]
AS
	SELECT 
		AnswerId,
		QuestionId,
		Description,
		CustomerId
	FROM
		[dbo].[Answer] a;

