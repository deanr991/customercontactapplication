﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve all questions from the
--				database
-- =============================================

CREATE PROCEDURE [dbo].[GetAllQuestions]
AS
	SELECT 
		QuestionId,
		Description
	FROM
		[dbo].[Question] q;

