﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve user information by
--				UserId
-- =============================================

CREATE PROCEDURE [dbo].[GetUserByUserId]
	@UserId INT
AS
	SELECT 
		UserId,
		Username,
		Password,
		Salt
	FROM
		[dbo].[User] u
	WHERE 
		u.UserId = @UserId;

