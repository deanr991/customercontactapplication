﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve customer information by
--				CustomerId
-- =============================================

CREATE PROCEDURE [dbo].[GetCustomerByCustomerId]
	@CustomerId INT
AS
	SELECT 
		CustomerId,
		FirstName,
		Surname,
		DateOfBirth,
		HomePhoneNumber,
		MobilePhoneNumber,
		Email,
		AddressId,
		Username
	FROM
		[dbo].[Customer] c
	WHERE 
		c.CustomerId = @CustomerId;

