﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Delete customer information by
--				AddressId
-- =============================================

CREATE PROCEDURE [dbo].[DeleteCustomer]
	@CustomerId INT
AS
	DECLARE @AddressId INT;

	BEGIN TRAN
		DELETE FROM
			Answer
		WHERE 
			CustomerId = @CustomerId;

		SELECT 
			@AddressId = a.AddressId
		FROM
			Address a
		INNER JOIN
			Customer c
		ON
			a.AddressId = c.AddressId
		WHERE 
			c.CustomerId = @CustomerId;

		DELETE FROM
			Customer
		WHERE 
			CustomerId = @CustomerId;

		DELETE FROM
			Address
		WHERE 
			AddressId = @AddressId;
	COMMIT

