﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Insert ErrorLog entry
-- =============================================

CREATE PROCEDURE [dbo].[InsertErrorLogEntry]
	@Source NVARCHAR(MAX),
	@RequestUri NVARCHAR(MAX),
	@Method NVARCHAR(MAX),
	@Exception NVARCHAR(MAX)
AS
	INSERT INTO
		[dbo].[ErrorLog]
	VALUES(
		@Source,
		@RequestUri,
		@Method,
		@Exception
	);

