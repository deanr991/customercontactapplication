﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Insert Customer
-- =============================================

CREATE PROCEDURE [dbo].[InsertCustomer]
	@Firstname NVARCHAR(500),
	@Surname NVARCHAR(500),
	@DateOfBirth DATE,
	@HomePhoneNumber NVARCHAR(500),
	@MobilePhoneNumber NVARCHAR(500),
	@Email NVARCHAR(500),
	@AddressId INT,
	@Username NVARCHAR(100)
AS
	INSERT INTO
		[dbo].[Customer]
	VALUES(
		@Firstname,
		@Surname,
		@DateOfBirth,
		@HomePhoneNumber,
		@MobilePhoneNumber,
		@Email,
		@AddressId,
		@Username
	);

	DECLARE @CustomerId INT = SCOPE_IDENTITY();

	SELECT TOP 1
		*
	FROM
		[dbo].[Customer]
	WHERE
		CustomerId = @CustomerId
