﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Insert Address
-- =============================================

CREATE PROCEDURE [dbo].[InsertAddress]
	@AddressLine1 NVARCHAR(500),
	@AddressLine2 NVARCHAR(500),
	@City NVARCHAR(500),
	@Postcode NVARCHAR(500),
	@Country NVARCHAR(500)
AS
	INSERT INTO
		[dbo].[Address]
	VALUES(
		@AddressLine1,
		@AddressLine2,
		@City,
		@Postcode,
		@Country
	);

	DECLARE @AddressId INT = SCOPE_IDENTITY();

	SELECT TOP 1
		*
	FROM
		[dbo].[Address]
	WHERE
		AddressId = @AddressId
