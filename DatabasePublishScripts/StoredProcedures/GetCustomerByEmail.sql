﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve customer information by
--				Email
-- =============================================

CREATE PROCEDURE [dbo].[GetCustomerByEmail]
	@Email NVARCHAR(500)
AS
	SELECT 
		CustomerId,
		FirstName,
		Surname,
		DateOfBirth,
		HomePhoneNumber,
		MobilePhoneNumber,
		Email,
		AddressId,
		Username
	FROM
		[dbo].[Customer] c
	WHERE 
		c.Email = @Email;

