﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Check if user with specified
--				username/password combination
--				exists in the database
-- =============================================

CREATE PROCEDURE [dbo].[GetValidatedUser]
	@Username NVARCHAR(100),
	@Password NVARCHAR(500)
AS
	SELECT 
		UserId,
		Username,
		Password,
		Salt
	FROM
		[dbo].[User] u
	WHERE 
		u.Username = @Username AND
		u.Password = @Password
