﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Insert User
-- =============================================

CREATE PROCEDURE [dbo].[InsertUser]
	@Username NVARCHAR(100),
	@Password NVARCHAR(500),
	@Salt NVARCHAR(500)
AS
	INSERT INTO
		[dbo].[User]
	VALUES(
		@Username,
		@Password,
		@Salt
	);

