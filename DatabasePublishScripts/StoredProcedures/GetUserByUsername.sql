﻿-- =============================================
-- Author:		Dean Ryan
-- Description:	Retrieve user information by
--				Username
-- =============================================

CREATE PROCEDURE [dbo].[GetUserByUsername]
	@Username NVARCHAR(100)
AS
	SELECT 
		UserId,
		Username,
		Password,
		Salt
	FROM
		[dbo].[User] u
	WHERE 
		u.Username = @Username;

