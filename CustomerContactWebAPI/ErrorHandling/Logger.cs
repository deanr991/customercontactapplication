﻿using BusinessEntities.Entities;
using BusinessServices.Services;
using DataLayer;
using DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace CustomerContactWebAPI.ErrorHandling
{
    /// <summary>
    /// All exceptions occuring within the web api are logged against the database
    /// </summary>
    public class Logger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            ErrorLog error = new ErrorLog();
            error.Source = "CustomerContactWebAPI";
            error.Method = context.Request.Method.ToString();
            error.RequestUri = context.Request.RequestUri.ToString();
            error.Exception = context.Exception.ToString();

            using (var _loggingServices = new LoggingServices(new AdoNetContext(), new ErrorLogRepository(), null))
            {
                _loggingServices.InsertError(error);
            }
        }
    }
}