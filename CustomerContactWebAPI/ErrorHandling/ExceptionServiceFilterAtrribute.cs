﻿using BusinessServices.Services;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace CustomerContactWebAPI.Helpers
{
    /// <summary>
    /// Wrap the controllers with this attribute to catch all exceptions so that the request source would
    /// only get friendly exception messages as a response
    /// </summary>
    public class ExceptionService : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.BadRequest);

            if (context.Exception is UserException ||
                context.Exception is CustomerException)
            { 
                response.Content = new StringContent(context.Exception.Message);
            }
            else
            {
                response.Content = new StringContent("Server error. Please report this to the IT team.");
            }

            throw new HttpResponseException(response);
        }
    }
}