﻿using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace CustomerContactWebAPI
{
    /// <summary>
    /// Apply a default authorize filter to all controllers hence making them impossible to be accessed without the request being authenticated
    /// </summary>
    public class FilterConfig
    {
        public static void RegisterHttpFilters(HttpFilterCollection filters)
        {
            filters.Add(new AuthorizeAttribute());
        }
    }
}
