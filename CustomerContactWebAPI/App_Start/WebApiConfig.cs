﻿using System.Web.Http;
using Microsoft.Practices.Unity;
using CustomerContactWebAPI.App_Start;
using BusinessServices.Services;
using DataLayer.Interfaces;
using DataLayer.Repositories;
using DataLayer;
using System.Web.Http.ExceptionHandling;
using CustomerContactWebAPI.ErrorHandling;
using System.Diagnostics;

namespace CustomerContactWebAPI
{
    public static class WebApiConfig
    {
        /// <summary>
        /// Register the default routes and the unity dependency objects
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            //Logging
            config.Services.Add(typeof(IExceptionLogger),
                new Logger());

            // Unity dependencies
            var container = new UnityContainer();
            container.RegisterType<ICustomerRepository, CustomerRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IAddressRepository, AddressRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IAnswerRepository, AnswerRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IQuestionRepository, QuestionRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IAdoNetContext, AdoNetContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IAdoNetUnitOfWork>(new InjectionFactory((c) => null));
            container.RegisterType<ICustomerServices, CustomerServices>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserServices, UserServices>(new HierarchicalLifetimeManager());
            container.RegisterType<ISurveyServices, SurveyServices>(new HierarchicalLifetimeManager());

            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{request}",
                defaults: new { request = RouteParameter.Optional }
            );
        }
    }
}
