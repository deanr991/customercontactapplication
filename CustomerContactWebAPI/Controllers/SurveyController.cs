﻿using System;
using BusinessEntities.Entities;
using BusinessServices.Services;
using System.Collections.Generic;
using System.Web.Http;
using System.Web;
using System.Net.Http.Headers;
using CustomerContactWebAPI.Helpers;

namespace CustomerContactWebAPI.Controllers
{
    /// <summary>
    /// Handle api requests accessing the survey business logic
    /// </summary>
    [ExceptionService]
    public class SurveyController : ApiController
    {
        private readonly ISurveyServices _surveyServices;

        public SurveyController(ISurveyServices surveyServices)
        {
            _surveyServices = surveyServices;
        }

        [HttpGet]
        public IEnumerable<Question> GetQuestionByQuestionId(int request)
        {
            return _surveyServices.GetQuestionByQuestionId(request);
        }

        [HttpGet]
        public IEnumerable<Question> GetAllQuestions()
        {
            return _surveyServices.GetAllQuestions();
        }

        [HttpGet]
        public IEnumerable<Answer> GetAllAnswers()
        {
            return _surveyServices.GetAllAnswers();
        }

        [HttpPost]
        public void CreateAnswer([FromBody]Answer request)
        {
            _surveyServices.CreateAnswer(request);
        }

        [HttpPost]
        public void CreateQuestion([FromBody]Question request)
        {
            _surveyServices.CreateQuestion(request);
        }

        [HttpGet]
        public IEnumerable<Answer> GetAnswerByCustomerId(int request)
        {
            return _surveyServices.GetAnswerByCustomerId(request);
        }

        protected override void Dispose(bool disposing)
        {
            _surveyServices.Dispose();
            base.Dispose(disposing);
        }
    }
}
