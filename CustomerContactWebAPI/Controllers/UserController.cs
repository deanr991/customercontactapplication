﻿using BusinessServices.Services;
using BusinessEntities.Entities;
using System.Collections.Generic;
using System.Web.Http;
using CustomerContactWebAPI.Helpers;

namespace CustomerContactWebAPI.Controllers
{
    /// <summary>
    /// Handle api requests accessing the user business logic
    /// </summary>
    [ExceptionService]
    public class UserController : ApiController
    {
        private readonly IUserServices _userServices;

        public UserController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        [HttpGet]
        public IEnumerable<User> GetUserByUserId(int request)
        {
            return _userServices.GetUserByUserId(request);
        }

        [HttpPost]
        public void CreateUser([FromBody]User request)
        {
            _userServices.CreateUser(request);
        }

        protected override void Dispose(bool disposing)
        {
            _userServices.Dispose();
            base.Dispose(disposing);
        }
    }
}
