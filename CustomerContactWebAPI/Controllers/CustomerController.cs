﻿using Newtonsoft.Json.Linq;
using BusinessEntities.Entities;
using System.Collections.Generic;
using BusinessServices.Services;
using System.Web.Http;
using CustomerContactWebAPI.Helpers;

namespace CustomerContactWebAPI.Controllers
{
    /// <summary>
    /// Handle api requests accessing the customer business logic
    /// </summary>
    [ExceptionService]
    public class CustomerController : ApiController
    {
        private readonly ICustomerServices _customerServices;

        public CustomerController(ICustomerServices customerServices)
        {
            _customerServices = customerServices;
        }

        [HttpGet]
        public IEnumerable<Customer> GetCustomerByCustomerId(int request)
        {
            return _customerServices.GetCustomerByCustomerId(request);
        }

        [HttpGet]
        public IEnumerable<Customer> GetCustomerBySurname(string request)
        {
            return _customerServices.GetCustomerBySurname(request);
        }

        [HttpGet]
        public IEnumerable<Customer> GetCustomerByEmail(string request)
        {
            return _customerServices.GetCustomerByEmail(request);
        }

        [HttpGet]
        public IEnumerable<Address> GetAddressByAddressId(string request)
        {
            return _customerServices.GetAddressByAddressId(int.Parse(request));
        }

        [HttpPost]
        public IEnumerable<Customer> CreateCustomer([FromBody] JObject request)
        {
            var newCustomer = request["Customer"].ToObject<Customer>();
            var newAddress = request["Address"].ToObject<Address>();
            var createdCustomer = _customerServices.CreateCustomer(newCustomer, newAddress);
            return createdCustomer;
        }

        [HttpDelete]
        public void DeleteCustomer(string request)
        {
            _customerServices.DeleteCustomer(int.Parse(request));
        }

        protected override void Dispose(bool disposing)
        {
            _customerServices.Dispose();
            base.Dispose(disposing);
        }
    }
}
