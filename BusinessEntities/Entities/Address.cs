﻿namespace BusinessEntities.Entities
{
    /// <summary>
    /// Address entity that the records from the Address database table are mapped to/from
    /// </summary>
    public class Address
    {
        public int? AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

    }
}
