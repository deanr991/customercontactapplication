﻿namespace BusinessEntities.Entities
{
    /// <summary>
    /// Question entity that the records from the Question database table are mapped to/from
    /// </summary>
    public class Question
    {
        public int? QuestionId { get; set; }
        public string Description { get; set; }
    }
}
