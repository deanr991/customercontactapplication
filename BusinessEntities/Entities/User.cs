﻿namespace BusinessEntities.Entities
{
    /// <summary>
    /// User entity that the records from the User database table are mapped to/from
    /// </summary>
    public class User
    {
        public int? UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
    }
}
