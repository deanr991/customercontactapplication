﻿namespace BusinessEntities.Entities
{
    /// <summary>
    /// Customer entity that the records from the Customer database table are mapped to/from
    /// </summary>
    public class Customer
    {
        public int? CustomerId { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string DateOfBirth { get; set; }
        public string HomePhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string Email { get; set; }
        public int? AddressId { get; set; }
        public string Username { get; set; }
    }
}
