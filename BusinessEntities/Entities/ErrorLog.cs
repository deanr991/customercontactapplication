﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities.Entities
{
    /// <summary>
    /// ErrorLog entity that the records from the ErrorLog database table are mapped to/from
    /// </summary>
    public class ErrorLog
    {
        public int Id { get; set; }
        public string Source { get; set; }
        public string Method { get; set; }
        public string RequestUri { get; set; }
        public string Exception { get; set; }
    }
}
