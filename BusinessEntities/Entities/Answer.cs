﻿namespace BusinessEntities.Entities
{
    /// <summary>
    /// Answer entity that the records from the Answer database table are mapped to/from
    /// </summary>
    public class Answer
    {
        public int? AnswerId { get; set; }
        public int? QuestionId { get; set; }
        public string Description { get; set; }
        public int? CustomerId { get; set; }
    }
}
